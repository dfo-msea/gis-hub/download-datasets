# download-datasets

__Main author:__  Cole Fields    
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
  + [Modules](#modules)
  + [Running the script](#running-the-script)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose of this script is to download every dataset from the GIS Hub, 
including those set to private (if the user has permissions to access them) 
as a back-up for storage. 


## Summary
Python script to log into the GIS Hub and download datasets. The metadata for 
every dataset will be downloaded in JSON format. If a dataset has a resource
attached to it (i.e. a zip file, or in some cases a single non-zip file), that
will be downloaded as well. Uses the ckanapi Python Module.


## Status
Completed


## Contents
Single python script meant to be run from the command line interface. 


## Methods
* Creates a CKAN session using `RemoteCKAN` and the user's API key.
* Retrieves a JSON dictionary of datasets using `action.package_search` which
includes private datasets (if user is permitted).
* Logs on to the GIS Hub with the user's username and password.
* Iterate through list of packages (datasets) and get the url, the package id, 
and resource id for each one
* the package id and resource id are needed as arguments for the 
`download_resource_data` function
* the url name is stripped to get the name of the zip file that will be used 
when downloading the dataset
* downloads the zip file (or resource) and the json metadata for each dataset


## Requirements
### Modules

* `requests`
* `urllib2`
* `json`
* `ckanapi`
* `os`

### Running the script
1. Open a command prompt

2. Make sure you are in drive where python is installed: 
`C:\`

3. Tell python to run the script:
`python \path\to\this\script\download-datasets.py`

If python is not included in the local machine's environmental variables,
specify the python path and then the path to the script such as:
`C:\Python27\python.exe C:\Work\download-datasets\download_datasets.py`

4. The script has 4 parameters required to run. Enter them when prompted:
* Private CKAN API Key (found at https://www.gis-hub.ca/user/ your-username)
* CKAN username
* CKAN password
* Path to output directory (script checks that directory exists)

## Caveats
* Modules must be installed to the version of python being used. Use a package
manager to install the modules.
* Only tested using Python 2.7.17
* Script will only access the current version of each dataset on the GIS-Hub, 
not previous versions.
* Script will only access datasets user has permission to access.

## Uncertainty
NA


## Acknowledgements
NA


## References
* https://github.com/ckan/ckanapi
