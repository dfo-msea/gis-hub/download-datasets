# import system modules
import requests
import urllib2
import json
from ckanapi import RemoteCKAN
import os

# url to CKAN Instance
CKAN_URL = "https://gis-hub.ca"

# users CKAN API Private Key (located on user account page on gis-hub.ca)
api_key = raw_input("Enter your CKAN API key: ")
u_name = raw_input("Enter your CKAN username: ")
p_word = raw_input("Enter your CKAN password: ")
output_dir = ""
while not os.path.isdir(output_dir):
    output_dir = raw_input("Enter a valid output directory path: ")

# change directory and print message
os.chdir(output_dir)
print("Datasets will be downloaded here: {}".format(os.getcwd()))

# use RemoteCKAN to connect to gis-hub site, authorize with api key
gis_hub = RemoteCKAN(CKAN_URL, apikey=api_key)

# load results from api call package_search into json dictionary
# same as the browser api call https://www.gis-hub.ca/api/3/action/package_search?include_private=True&rows=1000
response_dict = gis_hub.action.package_search(include_private=True, rows=1000)

# login function
def login(username, password):
    """
    Login to CKAN.

    Returns a ``requests.Session`` instance with the CKAN
    session cookie.
    """
    s = requests.Session()
    creds = {'login': username, 'password': password}
    # generic login extension
    url = CKAN_URL + '/login_generic'
    s.get(url)
    r = s.post(url, data=creds)
    if 'field-login' in r.text:
        # Response still contains login form
        raise RuntimeError('Login failed.')
    return s


def download_resource_data(session, pkg_id, res_id, file_name):
    """
    Uses the session logon information to downlaod datasets
    """
    url = '{ckan}/dataset/{pkg}/resource/{res}/download/'.format(
            ckan=CKAN_URL, pkg=pkg_id, res=res_id)
    package = session.get(url).content
    with open(file_name, "wb") as download:
        download.write(package)
    return package


# http session
session = login(u_name, p_word)

# iterate results
for i in response_dict['results']:
    """
    iterate through list of packages (datasets) and get the url, the package id, and resource id for each one
    the package id and resource id are needed as arguments for the download_resource_data function
    the url name is stripped to get the name of the zip file that will be used when downloading the dataset
    """
    print("Dumping json metadata for: {}".format(i["name"]))
    with open(i["name"] + ".json", "w") as meta:
        json.dump(i, meta)
    # make sure the dataset has a resource (i.e. is not just a metadata record)
    if len(i['resources']) > 0:
        # get package id
        package_id = i['id']
        # get resource id
        resource_id = str(i['resources'][0]['id'])
        # get url string and split to get name of zip
        zip_name = str(i['resources'][0]['url'].split("/")[-1])
        # print statement to user
        print("Downloading: {}".format(i["name"]))
        # call download function
        dataset = download_resource_data(session, package_id, resource_id, zip_name)

